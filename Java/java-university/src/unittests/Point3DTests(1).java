package unittests;

import static org.junit.Assert.*;

import org.junit.Test;

import primitives.*;
public class Point3DTests {

	@Test
	public void testAdd() {
		Coordinate x = new Coordinate(1);
		
		Vector v = new Vector(new Point3D(x,x,x));
		Point3D p = new Point3D(x,x,x);
		Point3D p2 = p.add(v);
		Point3D res=new Point3D(new Coordinate(2),new Coordinate(2),new Coordinate(2));
		
		assertEquals(res,p2);	
	}

	@Test
	public void testSubtractPoint3D() {
		Coordinate x = new Coordinate(1);
		Coordinate y = new Coordinate(1);
		Coordinate z = new Coordinate(1);
		
		Vector v = new Vector(new Point3D(x,y,z));
		Point3D p = new Point3D(new Coordinate(2),new Coordinate(2),new Coordinate(2));
		Point3D p2 = p.subtract(v);
		Point3D res=new Point3D(new Coordinate(1),new Coordinate(1),new Coordinate(1));
		
		assertEquals(res,p2);
	}

}
