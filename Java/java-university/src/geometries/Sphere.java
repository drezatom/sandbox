package geometries;

import primitives.*;


public  class Sphere implements Geometry
{
    Point3D center;
    double radius;

    public Sphere(Point3D center, double radius) {
    	 this.radius = radius;
    	 this.center = new Point3D(center);
    }
    

    public Sphere(Sphere r)
    {
        this.radius = r.radius;
        this.center = r.center;
    }

    public double getradius() {
        return radius;
    }

    public void setradius(double radius) {
        this.radius = radius;
    }

    public Point3D getCenter() {
        return center;
    }
    
    @Override
    public Vector getNormal(Point3D p) {
        Coordinate x1=new Coordinate(p.getX().get()-this.getCenter().getX().get());
        Coordinate y1=new Coordinate(p.getY().get()-this.getCenter().getY().get());
        Coordinate z1=new Coordinate(p.getZ().get()-this.getCenter().getZ().get());
        return new Vector(new Point3D(x1,y1,z1));
    }
}