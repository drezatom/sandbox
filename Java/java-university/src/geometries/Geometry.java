package geometries;

import primitives.*;

public interface Geometry {
    public Vector getNormal(Point3D pt);

}
