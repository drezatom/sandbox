package geometries;

import primitives.Point3D;
import primitives.Vector;

public class Plane implements Geometry {

    Point3D q0;
    primitives.Vector normal;

    public Plane(Point3D p1, Point3D p2, Point3D p3) {
        this.q0=p1;
        this.normal=null;
    }

    public Plane(Point3D p, Vector normal) {
        this.q0 = new Point3D(p);
        this.normal = new Vector(normal);
    }


    public Vector getNormal(Point3D p) {
        return normal;
    }

    public Vector getNormal() {
        return getNormal(null);
    }
}
