/**
 * 
 */
package unittests;

import static org.junit.Assert.*;

import org.junit.Test;

import primitives.*;

/**
 * @author Shlomo
 *
 */
public class VectorTests {

	/**
	 * Test method for {@link primitives.Vector#subtract(primitives.Vector)}.
	 */
	@Test
	public void testSubtract() {

		Vector v = new Vector(new Point3D(new Coordinate(1),new Coordinate(1),new Coordinate(1)));
		Vector v2 = new Vector(new Point3D(new Coordinate(2),new Coordinate(2),new Coordinate(2)));
		Vector v3 = v2.subtract(v);
		Vector res=new Vector(new Point3D(new Coordinate(1),new Coordinate(1),new Coordinate(1)));
		
		assertEquals(res,v3);
	}

	/**
	 * Test method for {@link primitives.Vector#add(primitives.Vector)}.
	 */
	@Test
	public void testAdd() {
		Coordinate x=new Coordinate(1);
		Coordinate y=new Coordinate(1);
		Coordinate z=new Coordinate(1);
		
		Vector v=new Vector(new Point3D(x,y,z));
		Vector v2=new Vector(new Point3D(x,y,z));
		Vector v3 = v2.add(v);
		Vector res=new Vector(new Point3D(new Coordinate(2),new Coordinate(2),new Coordinate(2)));
		
		assertEquals(res,v3);
	}

	/**
	 * Test method for {@link primitives.Vector#scale(double)}.
	 */
	@Test
	public void testScale() {
		Coordinate x=new Coordinate(1);
		Coordinate y=new Coordinate(1);
		Coordinate z=new Coordinate(1);
		
		Vector v=new Vector(new Point3D(x,y,z));
		
		double d=5;
		Vector v2 = v.scale(d);
		Vector res=new Vector(new Point3D(new Coordinate(5),new Coordinate(5),new Coordinate(5)));
		assertEquals(res,v2);
	}

	/**
	 * Test method for {@link primitives.Vector#dotProduct(primitives.Vector)}.
	 */
	@Test
	public void testDotProduct() {
		Coordinate x=new Coordinate(1);
		Coordinate y=new Coordinate(1);
		Coordinate z=new Coordinate(1);
		
		Vector v=new Vector(new Point3D(x,y,z));
		Vector p=new Vector(new Point3D(x,y,z));
		double res=p.dotProduct(v);
		double exp=3;
		assertEquals(res, exp, 0);
	}

	/**
	 * Test method for {@link primitives.Vector#crossProduct(primitives.Vector)}.
	 */
	@Test
	public void testCrossProduct() {
		Vector v1 = new Vector(1, 2, 3);

        // ============ Equivalence Partitions Tests ==============
        Vector v2 = new Vector(0, 3, -2);
        Vector vr = v1.crossProduct(v2);
        
        
        // TC01: Test that length of cross-product is proper (orthogonal vectors taken
        // for simplicity)
        assertEquals("crossProduct() wrong result length", v1.length() * v2.length(), vr.length(), 0.00001);

        // TC02: Test cross-product result orthogonality to its operands
        assertTrue("crossProduct() result is not orthogonal to 1st operand", Util.isZero(vr.dotProduct(v1)));
        assertTrue("crossProduct() result is not orthogonal to 2nd operand", Util.isZero(vr.dotProduct(v2)));

        // =============== Boundary Values Tests ==================
        // TC11: test zero vector from cross-productof co-lined vectors
        Vector v3 = new Vector(-2, -4, -6);
        assertThrows("crossProduct() for parallel vectors does not throw an exception",
                ArithmeticException.class, () -> v1.crossProduct(v3));
        // try {
        //     v1.crossProduct(v2);
        //     fail("crossProduct() for parallel vectors does not throw an exception");
        // } catch (Exception e) {}
	
	}

	/**
	 * Test method for {@link primitives.Vector#lengthSquared()}.
	 */
	@Test
	public void testLengthSquared() {
		Coordinate x = new Coordinate(0);
		Coordinate y = new Coordinate(0);
		Coordinate z = new Coordinate(2);
		
		Vector v = new Vector(new Point3D(x,y,z));
		double res=v.lengthSquared();
		assertEquals(4,res,0);
	}

	/**
	 * Test method for {@link primitives.Vector#length()}.
	 */
	@Test
	public void testLength() {
		Coordinate x = new Coordinate(0);
		Coordinate y = new Coordinate(0);
		Coordinate z = new Coordinate(2);
		
		Vector v = new Vector(new Point3D(x,y,z));
		double res=v.length();
		assertEquals(2,res,0);
	}

	/**
	 * Test method for {@link primitives.Vector#normalize()}.
	 */
	@Test
	public void testNormalize() {
		Coordinate x = new Coordinate(0);
		Coordinate y = new Coordinate(0);
		Coordinate z = new Coordinate(2);
		
		Vector v = new Vector(new Point3D(x,y,z));
		Vector vr = v.normalize();
		Vector exp = new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(1)));
		assertEquals(exp,vr);
	}

	/**
	 * Test method for {@link primitives.Vector#normalized()}.
	 */
	@Test
	public void testNormalized() {
		Coordinate x = new Coordinate(0);
		Coordinate y = new Coordinate(0);
		Coordinate z = new Coordinate(2);
		
		Vector v = new Vector(new Point3D(x,y,z));
		Vector vr = v.normalized();
		Vector exp = new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(1)));
		assertEquals(exp,vr);
	}

}
