package geometries;

import primitives.*;

class Cylinder extends Tube
{
    double height;

    public Cylinder(Ray axisRay, double radius, double height) {
    	super(axisRay, radius);
    	this.height = height;
    }

    public Vector getNormal() {
        return getNormal();
    }

}