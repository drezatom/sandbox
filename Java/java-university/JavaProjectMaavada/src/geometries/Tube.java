package geometries;

import primitives.*;



public class Tube implements Geometry {
    Ray axisRay;
    double radius;

    public Tube(Ray axisRay, double radius) {
    	this.radius = radius;
    	this.axisRay = new Ray(axisRay);
    }

    public Tube(Tube r) {
    	this.axisRay = r.axisRay;
    	this.radius = r.radius;
    }
    
    @Override
    public Vector getNormal(Point3D point) {
        return null;
    }
}
