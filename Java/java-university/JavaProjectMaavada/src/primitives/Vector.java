package primitives;


public class Vector {
    protected Point3D myPoint;

    public Vector(Coordinate a,Coordinate b,Coordinate c)
    {if ((a==null)&&(b==null)&&(c==null))
    {
        throw new IllegalArgumentException("Can't be a vector zero !");
    }
        Point3D temp = new Point3D(a,b,c);
        this.myPoint = temp;
    }
    
    public Vector(double first,double second,double thrid)
    {

        if ((first==0)&&(second==0)&&(thrid==0))
        {
            throw new IllegalArgumentException("Can't be first vector zero !");
        }
        else {
            Point3D point=new Point3D(first,second,thrid);
            this.myPoint=point;
        }
    }
    
    public Vector(Vector vect)
    {
        this.myPoint=vect.myPoint;
    }
    
    public Vector(Point3D point3D) {
        if ((point3D.x.get()==0)&&(point3D.y.get()==0)&&(point3D.z.get()==0))
        {
            throw new  IllegalAccessError();
        }
        else
            this.myPoint = point3D;
    }

    public void setPt(Point3D pt) {
        this.myPoint = pt;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Vector)) return false;
        Vector vector = (Vector) obj;
        return vector.myPoint.equals(this.myPoint);
    }
    
    public Vector subtract(Vector vec)  {
        return new Vector(this.myPoint.subtract(vec));
    }

    public Vector add(Vector vec)  {

        return new Vector(this.myPoint.add(vec));
    }


    public Vector scale(double scalary)
    {
        return new Vector((scalary*this.myPoint.x.get()),(scalary*this.myPoint.y.get()),(scalary*this.myPoint.z.get()));
    }
    
    public double dotProduct(Vector b)
    {

        return (this.myPoint.x.get()*b.myPoint.x.get()) + (this.myPoint.y.get()*b.myPoint.y.get())+ (this.myPoint.z.get()*b.myPoint.z.get());
    }
    
    public Vector crossProduct(Vector b)
    {

        if(b.myPoint.x.get()%this.myPoint.x.get()==0&&b.myPoint.y.get()%this.myPoint.y.get()==0&&b.myPoint.z.get()%this.myPoint.z.get()==0)
        {
            throw new  ArithmeticException("devide by efess");
        }
        return  new Vector((this.myPoint.y.get()*b.myPoint.z.get())-(this.myPoint.z.get()*b.myPoint.y.get())
                ,(this.myPoint.z.get()*b.myPoint.x.get())-(this.myPoint.x.get()*b.myPoint.z.get()),
                (this.myPoint.x.get()*b.myPoint.y.get())-this.myPoint.y.get()*b.myPoint.x.get());

    }

    public double lengthSquared()
    {

        return (this.myPoint.x.get())*(this.myPoint.x.get())+(this.myPoint.y.get())*(this.myPoint.y.get())+(this.myPoint.z.get())*(this.myPoint.z.get());
    }
    
    public double 	length()
    {
        return Math.sqrt( (this.myPoint.x.get())*(this.myPoint.x.get())+(this.myPoint.y.get())*(this.myPoint.y.get())+(this.myPoint.z.get())*(this.myPoint.z.get()));
    }
    
    public  Vector normalize()
    {
        double xx=this.myPoint.x.get();
        double yy=this.myPoint.y.get();
        double zz=this.myPoint.z.get();
        //Point3D temp = new Point3D(xx,yy,zz);
        Vector nvx = new Vector(new Point3D(xx,yy,zz));
        if (nvx.length() == 0) {
            throw new ArithmeticException("Error! divide by Zero");
        }

        this.myPoint.x = new Coordinate(xx/nvx.length());
        this.myPoint.y = new Coordinate(yy/nvx.length());
        this.myPoint.z = new Coordinate(zz/nvx.length());
        return this;
    }
    
    public Vector normalized()
    {
        return  new Vector (this.normalize());
    }

    @Override
    public String toString() {
        return "Vector{" +
                "a=" + myPoint +
                ", b=" +
                ", _z=" +
                '}';
    }


    public Point3D getA() {
        return myPoint;
    }
}
