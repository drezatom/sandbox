package primitives;

import primitives.*;

public class Ray {
    protected Point3D p0;
    protected Vector dir;

    public Ray(Point3D p0,Vector dir) {
        this.p0 = p0;
        this.dir=dir;

    }
    
    public Ray(Ray ray) {
        this.p0 = ray.p0;
        this.dir= ray.dir;

    }


    public Point3D get_p0() {
        return p0;
    }

    public void set_p0(Point3D p0) {
        this.p0 = p0;
    }

    public Vector get_dir() {
        return dir;
    }

    public void set_dir(Vector dir) {
        this.dir = dir;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Ray)) return false;
        Ray ray = (Ray) obj;
        return this.p0.equals(ray.p0) &&
                this.dir.equals(ray.dir);
    }

    @Override
    public String toString() {
        return "Ray{" +
                "Point3D=" + p0 +
                ", Vector=" + dir +
                ", Point3D="  +
                '}';
    }
}

