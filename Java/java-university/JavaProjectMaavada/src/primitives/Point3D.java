package primitives;

// import java.util.Objects;

public class Point3D {
    protected Coordinate x;
    protected Coordinate y;
    protected Coordinate z;
    public static final Point3D ZERO=new Point3D(0,0,0);



    public static Point3D getZERO() {
        return ZERO;
    }

    public Point3D add (Vector a)
    {
        Point3D temp = new Point3D((this.x.get()+a.myPoint.x.get()),(this.y.get()+a.myPoint.y.get()),(this.z.get()+a.myPoint.z.get()));
        return temp ;
    }

    public Point3D subtract (Vector a)
    {
        Point3D temp=new Point3D((this.x.get()-a.myPoint.x.get()),(this.y.get()-a.myPoint.y.get()),(this.z.get()-a.myPoint.z.get()));
        return temp;
    }
    
    public Vector subtract (Point3D a)
    {
        Vector temp=new Vector(this.x.get()-a.x.get(),this.y.get()-a.y.get(),this.z.get()-a.z.get());
        return temp;
    }

    
    
    public double distanceSquared (Point3D a ,Point3D b) //distance entre deux points 3D 
    {
        double distance=  ((a.x.get()-b.x.get())*(a.x.get()-b.x.get())+(a.y.get()-b.y.get())*(a.y.get()-b.y.get())+(a.z.get()-b.z.get())*(a.z.get()-b.z.get()));
        return distance;
    }

    public double distance (Point3D a , Point3D b)

    {
        double y=distanceSquared(a,b);
        return Math.sqrt(y);
    }

    public Point3D(double x, double y, double z) {
        this.x = new Coordinate(x);
        this.y=new Coordinate(y);
        this.z=new Coordinate(z);
    }
    
    public Point3D(Coordinate a,Coordinate b,Coordinate c)
    {
        this.x=a;
        this.y=b;
        this.z=c;

    }

    public Point3D (Point3D a) {
        this.x=a.x;
        this.y=a.y;
        this.z=a.z;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point3D)) return false;
        Point3D point3D = (Point3D) o;
        return x.equals(point3D.x) && y.equals(point3D.y) && z.equals(point3D.z);
    }


    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    public Coordinate getZ() {
        return z;
    }
}
