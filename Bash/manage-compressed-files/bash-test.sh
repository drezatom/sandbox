#!/bin/bash

TIMEOUT=48

zip_file() {
  local file=$1
  gzip "$file"
  mv "${file}.gz" "fc-$(basename "${file}.gz")"
}

move_compressed_file() {
  local file=$1
  mv "$file" "fc-$(basename "$file")"
  touch "fc-$(basename "$file")"
}

delete_old_files() {
  local file=$1
  local age=$((TIMEOUT * 3600))
  if [[ $(find "$file" -mmin +$((TIMEOUT * 60)) 2>/dev/null) ]]; then
    rm "$file"
  fi
}

process() {
  local path=$1

  if [ -d "$path" ]; then
    for file in "$path"/*; do
      [ -e "$file" ] || continue
      if [ -d "$file" ]; then
        [ "$RECURSIVE" == "true" ] && process "$file"
      else
        process_file "$file"
      fi
    done
  else
    process_file "$path"
  fi
}

process_file() {
  local file=$1

  if [[ "$file" == fc-* ]]; then
    delete_old_files "$file"
    return
  fi

  if file "$file" | grep -qE 'gzip|bzip2|xz|compress'; then
    move_compressed_file "$file"
  else
    zip_file "$file"
  fi
}

while getopts "rt:" opt; do
  case $opt in
  r) RECURSIVE=true ;;
  t) TIMEOUT=$OPTARG ;;
  *)
    echo "Usage: $0 [-r] [-t ###] file [file...]"
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

if [ $# -eq 0 ]; then
  echo "Usage: $0 [-r] [-t ###] file [file...]"
  exit 1
fi

for path in "$@"; do
  process "$path"
done
