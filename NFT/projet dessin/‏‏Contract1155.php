pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/utils/Strings.sol";


contract NFT is ERC1155 {

  constructor() ERC1155('http://error405.esy.es/testfolder/{id}.json') {
    for(uint i = 1 ; i <= 100 ; i++) {
        _mint(msg.sender, i, 1, bytes(abi.encodePacked("Arme number #", Strings.toString(i))));
    }
  }

  function uri (uint _tokenId) override public pure returns(string memory) {
      return string(abi.encodePacked(
          "http://error405.esy.es/testfolder/",
          Strings.toString(_tokenId),
          ".json"
      ));
  }

  function name() public pure returns(string memory) {
      string memory name = "Collection of weapon";
      return name;
  }
}