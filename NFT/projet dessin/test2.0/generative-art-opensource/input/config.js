const fs = require("fs");
const width = 1000;
const height = 1000;
const dir = __dirname;
const description = "By Drezatom";
const baseImageUri = "http://error405.esy.es/testfolder";
const startEditionFrom = 1;
const endEditionAt = 100;
const editionSize = 100;
const raceWeights = [
  {
    value: "arme",
    from: 1,
    to: editionSize,
  },
];

const races = {
  arme: {
    name: "Skull",
    layers: [
      
      {
        name: "fond",
        elements: [
          {
            id: 0,
            name: "fond 3",
            path: `${dir}/fond/fond#1.png`,
            weight: 100,
          },
          {
            id: 1,
            name: "fond 1",
            path: `${dir}/fond/fond#3.png`,
            weight: 66,
          },
          {
            id: 2,
            name: "fond 2",
            path: `${dir}/fond/fond#2.png`,
            weight: 33,
          },
        ],
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "firstparti",
        elements: [
          {
            id: 0,
            name: "firstparti 4",
            path: `${dir}/firstparti/firstparti#4.png`,
            weight: 100,
          },
          {
            id: 1,
            name: "firstparti 1",
            path: `${dir}/firstparti/firstparti#1.png`,
            weight: 75,
          },
          {
            id: 2,
            name: "firstparti 2",
            path: `${dir}/firstparti/firstparti#2.png`,
            weight: 50,
          },
          {
            id: 3,
            name: "firstparti 3",
            path: `${dir}/firstparti/firstparti#3.png`,
            weight: 25,
          },
        ],
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "manche",
        elements: [
          {
            id: 0,
            name: "manche 7",
            path: `${dir}/manche/manche#7.png`,
            weight: 100,
          },
          {
            id: 1,
            name: "manche 1",
            path: `${dir}/manche/manche#1.png`,
            weight: 86,
          },
          {
            id: 2,
            name: "manche 2",
            path: `${dir}/manche/manche#2.png`,
            weight: 72,
          },
          {
            id: 3,
            name: "manche 3",
            path: `${dir}/manche/manche#3.png`,
            weight: 58,
          },
          {
            id: 4,
            name: "manche 4",
            path: `${dir}/manche/manche#4.png`,
            weight: 44,
          },
          {
            id: 5,
            name: "manche 5",
            path: `${dir}/manche/manche#5.png`,
            weight: 30,
          },
          {
            id: 6,
            name: "manche 6",
            path: `${dir}/manche/manche#6.png`,
            weight: 15,
          },
        ],
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "secondparti",
        elements: [
          {
            id: 0,
            name: "secondparti 10",
            path: `${dir}/secondparti/secondparti#10.png`,
            weight: 100,
          },
          {
            id: 1,
            name: "secondparti 1",
            path: `${dir}/secondparti/secondparti#1.png`,
            weight: 90,
          },
          {
            id: 2,
            name: "secondparti 2",
            path: `${dir}/secondparti/secondparti#2.png`,
            weight: 80,
          },
          {
            id: 3,
            name: "secondparti 3",
            path: `${dir}/secondparti/secondparti#3.png`,
            weight: 70,
          },
          {
            id: 4,
            name: "secondparti 4",
            path: `${dir}/secondparti/secondparti#4.png`,
            weight: 60,
          },
          {
            id: 5,
            name: "secondparti 5",
            path: `${dir}/secondparti/secondparti#5.png`,
            weight: 50,
          },
          {
            id: 6,
            name: "secondparti 6",
            path: `${dir}/secondparti/secondparti#6.png`,
            weight: 40,
          },
          {
            id: 7,
            name: "secondparti 7",
            path: `${dir}/secondparti/secondparti#7.png`,
            weight: 30,
          },
          {
            id: 8,
            name: "secondparti 8",
            path: `${dir}/secondparti/secondparti#8.png`,
            weight: 20,
          },
          {
            id: 9,
            name: "secondparti 9",
            path: `${dir}/secondparti/secondparti#9.png`,
            weight: 10,
          },
        ],
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "rarete",
        elements: [
          {
            id: 0,
            name: "Gold",
            path: `${dir}/rarete/gold.png`,
            weight: 100,
          },
        ],
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "attaque",
        elements: [
          {
            id: 0,
            name: "Attaque 100",
            path: `${dir}/attaque/attaque1.png`,
            weight: 100,
          },
          {
            id: 1,
            name: "Attaque 200",
            path: `${dir}/attaque/attaque2.png`,
            weight: 50,
          },
        ],
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
    ],
  },
};

module.exports = {
  width,
  height,
  description,
  baseImageUri,
  editionSize,
  startEditionFrom,
  endEditionAt,
  races,
  raceWeights,
};
